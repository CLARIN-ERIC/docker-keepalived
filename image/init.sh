#!/bin/bash

PRIORITY_MASTER=150
PRIORITY_BACKUP=100
CONF_FILE="/etc/keepalived/keepalived.conf"
MASTER_VI2_CLAIMIP_SCRIPT="/usr/bin/sync_ip.sh"

# Check if all variables are set
if [ -z ${ETH_IFACE+x} ]; then
    echo "Vrrp network interface (ETH_IFACE) is missing."
    exit 1
fi
if [ -z ${PASSWORD+x} ]; then
    echo "Password (PASSWORD) is missing."
    exit 1
fi
if [ -z ${SRC_IP+x} ]; then
    echo "Source IP address (SRC_IP) is missing."
    exit 1
fi
if [ -z ${CAST_IP+x} ]; then
    echo "Cast IP address (CAST_IP) is missing."
    exit 1
fi
if [ -z ${ROLE+x} ]; then
    echo "Instance role (ROLE) is missing. Allowed values: MASTER, BACKUP"
    exit 1
fi
if [ -z ${HAIP_NAME+x} ]; then
    echo "Transip haip name is missing."
    exit 1
fi
if [ -z ${VPS_NAME+x} ]; then
    echo "Transip ip vps name is missing."
    exit 1
fi

echo "Keepalived settings:"
echo "  Vrrp iface    : ${ETH_IFACE}"
echo "  Password      : xxxxxxxx"
echo "  Source IP     : ${SRC_IP}"
echo "  Cast IP{      : ${CAST_IP}"
echo "  Server role   : ${ROLE}"
echo ""
echo "Switch IP settings:"
echo "  Haip name     : ${HAIP_NAME}"
echo "  VPS name      : ${VPS_NAME}"
echo ""

# Update keepalived config
replaceVarInFile "ETH_IFACE" "${ETH_IFACE}" "${CONF_FILE}"
replaceVarInFile "AUTH_PASSWORD" "${PASSWORD}" "${CONF_FILE}"
replaceVarInFile "SRC_IP" "${SRC_IP}" "${CONF_FILE}"
replaceVarInFile "CAST_IP" "${CAST_IP}" "${CONF_FILE}"

# Remove own interface from interface tracking 
sed -i "/track_interface/,/${ETH_IFACE}|/ l/^[ ]*${ETH_IFACE}[ ]*$/d" ${CONF_FILE}

# Configure MASTER and BACKUP with reverse priorities
if [ "${ROLE}" == "MASTER" ]; then
    replaceVarInFile "PRIORITY1" "${PRIORITY_MASTER}" "${CONF_FILE}"
    replaceVarInFile "PRIORITY2" "${PRIORITY_BACKUP}" "${CONF_FILE}"
    # Only the MASTER claims the HAIP when on its VI_2 virtual router
    replaceVarInFile "MASTER_VI2_NOTIFY_MASTER" "notify_master ${MASTER_VI2_CLAIMIP_SCRIPT}" "${CONF_FILE}"
elif [ "${ROLE}" == "BACKUP" ]; then
    replaceVarInFile "PRIORITY1" "${PRIORITY_BACKUP}" "${CONF_FILE}"
    replaceVarInFile "PRIORITY2" "${PRIORITY_MASTER}" "${CONF_FILE}"
    # Only the MASTER claims the HAIP when on its VI_2 virtual router
    replaceVarInFile "MASTER_VI2_NOTIFY_MASTER" "" "${CONF_FILE}"
fi

# Clean any stale pid files before starting
echo "Cleaning .pid files" && rm -rf /var/run/keepalived/*.pid