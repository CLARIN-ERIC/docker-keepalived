#!/bin/bash
set -e

HAIP_NAME="${HAIP_NAME:-haip1}"
VPS_NAME="${VPS_NAME:-vps1}"
MASTER_EXTERNAL_IP="${MASTER_EXTERNAL_IP:-0.0.0.0}"
max_retry=${MAX_RETRY:-10}
send_email=${SEND_EMAIL:-false}

# Debug mode
if [ "$1" == "-v" ]; then
    set -x
fi

# If the script is being run by keepalived print the output to the keepalived log
if [ -f /var/run/keepalived/vrrp.pid ]; then
    source "$(dirname "$0")/redirect2keepalived_log.sh"
fi

# When script is running on the BACKUP instance, check if the MASTER is operational ad give up if not. This avoid flapping due to keepalived internal network failures
if [ "${ROLE}" == "BACKUP" ]; then
    if [ "$(curl --connect-timeout 1 --max-time 1 --retry 0 --write-out "%{http_code}\n" --silent --output /dev/null http://"${MASTER_EXTERNAL_IP}"/)" == "200" ]; then
        echo "WARNING: MASTER instance running on ${MASTER_EXTERNAL_IP}, seems to be operational. Skipping pull of HAIP address."
        exit 1
    fi
fi

echo "Switching ip address to ${VPS_NAME}..."

i=0
while [ "$i" -le "${max_retry}" ]
do
    # Skip if HAIP is already assigned to this instance (avoids flapping)
    if ! check_output=$(/usr/bin/transip haip check -k /app/private_key.pem --name "${HAIP_NAME}" -V "${VPS_NAME}"); then

        if ! change_output=$(/usr/bin/transip haip change -k /app/private_key.pem --name "${HAIP_NAME}" --vps "${VPS_NAME}"); then
            echo "ERROR: Transip API client returned an unexpected error"
            echo "${change_output}"
            i=$((i+1))
            if [ "$i" -le "${max_retry}" ]
            then
                echo "Retrying in 5 seconds..."
                sleep 5
            else
                echo "HA-IP FAILED to switch VPS to: ${VPS_NAME}"
                mail_text="Subject: HAIP: [${HAIP_NAME}] FAILED to switch attached VPS to: [${VPS_NAME}]\n\nFailed to switch VPS attached to HA-IP:[${HAIP_NAME}] to: [${VPS_NAME}] after ${i} retries. The following error was captured:\n\n----\n${change_output}\n----"
                if "${send_email}"; then
                    echo "Sending warning email... Text:"
                    echo "$mail_text"
                    builtin echo -e "$mail_text" | sendmail -f "keepalived-proxy_at_${VPS_NAME}@clarin.eu" root
                fi
            fi
        else
            echo "HA-IP successfully switched VPS to: ${VPS_NAME}"
            mail_text="Subject: HAIP: [${HAIP_NAME}] switched attached VPS to: [${VPS_NAME}]\n\nSuccessfully switched VPS attached to HA-IP: [${HAIP_NAME}] to: [${VPS_NAME}] after ${i} retries. The following output was captured:\n\n----\n${change_output}\n----"
            if "${send_email}"; then
                echo "Sending success email... Text:"
                echo "$mail_text"
                builtin echo -e "$mail_text" | sendmail -f "keepalived-proxy_at_${VPS_NAME}@clarin.eu" root
            fi
            break
        fi

    else
        echo "${check_output}"
        echo "Already on ${VPS_NAME}. Skipping pull of HAIP address."
        exit 1
    fi
done