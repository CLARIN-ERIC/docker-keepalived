#!/bin/bash
set -e

HAIP_NAME="${HAIP_NAME:-haip1}"
VPS_NAME="${VPS_NAME:-vps1}"

# Debug mode
if [ "$1" == "-v" ]; then
    set -x
fi

# If the script is being run by keepalived print the output to the keepalived log
if [ -f /var/run/keepalived/vrrp.pid ]; then
    source "$(dirname "$0")/redirect2keepalived_log.sh"
fi

if output=$(/usr/bin/transip haip check -k /app/private_key.pem --name "${HAIP_NAME}" -V "${VPS_NAME}"); then
        echo "${output}"
        echo "Already on ${VPS_NAME}. Skipping HAIP address synchronization."
        exit 1
else
        echo "${output}"
        echo "MASTER and selected VPS out of sync! Switching IP address back to ${VPS_NAME}..."
        pull_ip.sh
fi